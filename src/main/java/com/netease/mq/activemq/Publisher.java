package com.netease.mq.activemq;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;

/**
 * 消息發佈者
 * @author hzlengzhaoyang
 *
 */
public class Publisher {
	private Session session = null;
	private Connection connection = null;
	
	public Publisher() throws JMSException {
		connection = MQConnectionFactory.getInstance().createConnection();
		connection.start();
		session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
	}
	
	public String send(String topic, String message) throws JMSException {
		if(topic == null) {
			return null;
		}
		
		Destination destination = session.createTopic(topic);
		Message msg = session.createMapMessage();
		msg.setStringProperty("message", message);
		MessageProducer producer = session.createProducer(null); 
		producer.send(destination, msg);
		session.commit();
		
		return message;
	}
	
	public void close() {
		if(connection != null) {
			try {
				connection.close();
			} catch (JMSException e) {
				e.printStackTrace();
			} 
		}
	}
	
}
