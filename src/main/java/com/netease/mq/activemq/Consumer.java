package com.netease.mq.activemq;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;

/**
 * 消息發佈者
 * @author hzlengzhaoyang
 *
 */
public class Consumer implements MessageListener{
	private MessageConsumer consumer = null;
	private Session session = null;
	private Connection connection = null;
	
	public Consumer(String topic) throws JMSException {
		connection = MQConnectionFactory.getInstance().createConnection();
		connection.start();
		session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
		receive(topic);
	}
	
	public void receive(String topic) throws JMSException {
		if(topic == null) {
			return;
		}
		
		Destination destination = session.createTopic(topic);
		consumer = session.createConsumer(destination);
		consumer.setMessageListener(this);
	}

	public void close() {
		if(connection != null) {
			try {
				connection.close();
			} catch (JMSException e) {
				e.printStackTrace();
			} 
		}
	}

	public void onMessage(Message message) {
		// TODO Auto-generated method stub
		if(message instanceof MapMessage) {
			try {
				System.out.print(message.getStringProperty("message"));
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
