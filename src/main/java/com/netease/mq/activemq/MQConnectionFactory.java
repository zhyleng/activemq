package com.netease.mq.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

public class MQConnectionFactory {
	/** volatile 保证对象的读写可见性  */
	private static volatile ActiveMQConnectionFactory factory;
	
	/** double check */
	public static ActiveMQConnectionFactory getInstance() {
		if(factory == null) {
			synchronized (MQConnectionFactory.class) {
				if(factory == null) {
					factory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_USER, ActiveMQConnectionFactory.DEFAULT_PASSWORD, "tcp://127.0.0.1:61616");
				}
			}
		}
		return factory;
	}
}
