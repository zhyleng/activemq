package com.netease.mvc;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.apache.log4j.MDC;
import java.util.Date;

import org.apache.log4j.NDC;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
	public static void main(String[] args) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date date = df.parse("2015-11-01");
		System.out.print(date.getTime());

		BigDecimal b = new BigDecimal(11.5d);
		b = b.add(new BigDecimal(2));
		System.out.println(b);

		BigDecimal d = new BigDecimal(12);
		BigDecimal c = new BigDecimal(13.5d);
		System.out.println(c.equals(b));
		System.out.println(d.getClass().getName());

		PropertyConfigurator.configure(App.class.getClassLoader().getResourceAsStream("log4j/log4j.properties"));    
		Logger log = LoggerFactory.getLogger(App.class);
		MDC.put("username", "桔子");//线程独立的 ThreadLocal
		NDC.push("juzi");//hashTable.put(Thread.currentThread, new Stack());
		log.debug("debug{}", 1);
		log.info("info");
		log.warn("warn");
		log.error("error");

		log.error("hello world");
		log.trace("trace");
		NDC.remove();
		MDC.clear();

		new Thread() {
			public void run() {
				ThreadContext.setUserId(100);
				r();
			}
		}.start();

		System.out.println(ThreadContext.getUserId());
		boolean flag = true;
		if(flag) {
			throw new Exception("Test Exception");
		}
		System.out.println(ThreadContext.getUserId());
	}

	public static void r() {
		System.out.println(ThreadContext.getUserId() + Thread.currentThread().getName());
		boolean flag = true;
		if(flag) {
			System.out.println("test");
		} else {
			System.out.println();
		}
	}

}



