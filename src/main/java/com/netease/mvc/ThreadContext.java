package com.netease.mvc;

import java.util.HashMap;
import java.util.Map;

public class ThreadContext {
	public static final ThreadLocal<Map<String, Object>> userIdThreadLocal = new ThreadLocal<Map<String, Object>>() {
		protected Map<String,Object> initialValue() {
			return new HashMap<String, Object>(); 
		};
	};
	
	public static void setUserId(long userId) {
		Map<String, Object> map = userIdThreadLocal.get();
		if(map == null) {
			map = new HashMap<String, Object>();
		}
		map.put("user_id", userId);
		userIdThreadLocal.set(map);
	}
	
	public static Long getUserId() {
		Map<String, Object> map = userIdThreadLocal.get();
		return map.get("user_id") == null ? null : (Long)map.get("user_id");
	}
	
}
