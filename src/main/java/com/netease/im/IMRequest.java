package com.netease.im;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class IMRequest {
	private static final int HTTP_TIMEOUT = 30*1000;
	private static final String HTTP_METHOD_POST = "POST";
	private static final String HTTP_CHARSET = "UTF-8";
	private static final String HTTP_CONTENT_TYPE = "application/x-www-form-urlencoded;charset=utf-8";
	private static final String IM_ACCOUNT_CREATE = "https://api.netease.im/nimserver/user/create.action";
	private static final String APPKEY_KEY = "AppKey";
	private static final String NONCE_KEY = "Nonce";
	private static final String CURTIME_KEY = "CurTime";
	private static final String CHECKSUM_KEY = "CheckSum";
	private static final String CONTENT_TYPE_KEY = "Content-Type";

	private static final String APPKEY_VALUE = "6078ba08d1ebcc372d1ada46298bc13d";
	private static final String APPSECRET_VALUE = "ef1ac6d455fa";

	public static void main(String[] args) {
		System.out.println(JSON.toJSONString(createAccount(MD5.md5("12312312311", 32), "桔子zji", null, null)));
	}
	
	/**
	 * 创建账号
	 */
	public static IMResponse createAccount(String accid, String nickname, String icon, String token) {
		URL createAccountUrl = null;
		IMResponse resp = null;
		try {
			createAccountUrl = new URL(IM_ACCOUNT_CREATE);
			HttpURLConnection conn = (HttpURLConnection) createAccountUrl.openConnection();
			/** 设置参数 */
			conn.setConnectTimeout(HTTP_TIMEOUT);
			conn.setRequestMethod(HTTP_METHOD_POST);
			conn.setDoOutput(true);
			conn.setUseCaches(false);

			/** 请求实体 */
			StringBuffer entity = new StringBuffer("");
			entity.append("accid=" + accid + "&");
			entity.append("name=" + nickname + "&");
			if(icon != null) {
				entity.append("icon=" + icon + "&");
			}
			if(token != null) {
				entity.append("token=" + token + "&");
			}
			
			entity = entity.deleteCharAt(entity.length()-1);
			System.out.println("entity: " + entity);

			/** 设置请求头 */
			String nonce = UUID.randomUUID().toString();
			long curSeconds = System.currentTimeMillis()/1000;
			String checkSum = sha1(APPSECRET_VALUE + nonce + curSeconds);
			conn.addRequestProperty(APPKEY_KEY, APPKEY_VALUE);
			conn.addRequestProperty(NONCE_KEY, nonce);
			conn.addRequestProperty(CURTIME_KEY, String.valueOf(curSeconds));
			conn.addRequestProperty(CHECKSUM_KEY, checkSum);
			conn.addRequestProperty(CONTENT_TYPE_KEY, HTTP_CONTENT_TYPE);

			/** 连接  */
			conn.connect();

			/** POST实体 */
			OutputStream os = conn.getOutputStream();
			os.write(entity.toString().getBytes(HTTP_CHARSET));
			os.flush();
			os.close();

			String response = getText(conn);
			
			JSONObject obj = JSON.parseObject(response);
			if(obj.getIntValue("code") == 200) {
				resp = JSON.parseObject(obj.getString("info"), IMResponse.class);
			}
			
			System.out.println("response: " + response);
		} catch (MalformedURLException  e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resp;
	}

	/**
	 * 对字符串进行 SHA1 加密
	 * @param str
	 *            待加密字符串
	 * @return 加密后字符串
	 */
	public static String sha1(String str) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };
		MessageDigest sha1 = null;
		try {
			sha1 = MessageDigest.getInstance("SHA-1");
			sha1.update(str.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		byte[] encodedValue = sha1.digest();
		int j = encodedValue.length;
		char finalValue[] = new char[j * 2];
		int k = 0;
		for (int i = 0; i < j; i++) {
			byte encoded = encodedValue[i];
			finalValue[k++] = hexDigits[encoded >> 4 & 0xf];
			finalValue[k++] = hexDigits[encoded & 0xf];
		}

		return new String(finalValue);
	}
	
	/**
	 * 获得连接请求的返回数据
	 * @param conn
	 * @return 字符串
	 */
	private static String getText(HttpURLConnection conn) throws IOException {
		StringBuilder text = new StringBuilder();

		InputStream is = null;
		InputStreamReader sr = null;
		BufferedReader br = null;

		int code = conn.getResponseCode();

		is = code >= 400 ? conn.getErrorStream() : conn.getInputStream();

		sr = new InputStreamReader(is, "UTF-8");
		br = new BufferedReader(sr);

		char[] chars = new char[4096];
		int length = 0;

		while ((length = br.read(chars)) != -1) {
			text.append(chars, 0, length);
		}
		if (br != null) {
			br.close();
			br = null;
		}
		if (sr != null) {
			sr.close();
			sr = null;
		}
		if (is != null) {
			is.close();
			is = null;
		}
		if (code >= 400)
			throw new IOException(text.toString());

		return text.toString();
	}

}
