import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class Prototype implements Cloneable, Serializable{
	private static final long serialVersionUID = 1L;
	
	int i;
	String s = new String("abc");

	public Prototype clone() throws CloneNotSupportedException {
		return (Prototype)super.clone();
	}
	
	public Prototype deepClone() throws IOException, ClassNotFoundException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream outObject = new ObjectOutputStream(out);
		outObject.writeObject(this);
		
		ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
		ObjectInputStream inObject = new ObjectInputStream(in);
		return (Prototype) inObject.readObject();
	}
	
	public static void main(String[] args) throws CloneNotSupportedException, ClassNotFoundException, IOException {
		Prototype t = new Prototype();
		System.out.println(t.s == t.clone().s);
		System.out.println(t.s == t.deepClone().s);
	}
}
